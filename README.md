# git version control tester
In this repository, I will get accustomed to git. Note that the purpose of this repository is not to get a working single-player verison of siete y medio. It is merely to learn how to use git and incorporate it into my workflow. 

## process
#### understanding git
I learned how to stage and commit files using git on visual studios 2019. This process was confusing at first, but I found a few helpful [tutorials](https://docs.microsoft.com/en-us/azure/devops/repos/git/gitworkflow?view=azure-devops) on how to incorporate git into my workflow as well as [how to push](https://docs.microsoft.com/en-us/azure/devops/repos/git/pushing?view=azure-devops&tabs=visual-studio) the source code to a remote repository.
#### branching
I attempted branching by creating a total of 4 branches. I first created the testerbranch then merged this to the master branch. Then, I created the experimental branch as well as the experimental2 branch then mereged these three branches together. 
#### merging
While merging I sometimes experienced conflicts when the changes to experimental branch and the main branch coincided. To handle this, I updated the code to either keep the changes from the experimental or main branch. This process was particularly confusing, but [this tutorial](https://docs.microsoft.com/en-us/azure/devops/repos/git/merging?view=azure-devops&tabs=visual-studio) was helpful. 
#### renaming bitbucket
I wanted to test what would happen if I renamed the bitbucket repository. I learned that by renaming the bitbucket repository, the link changed which prevented my files from being pushed from the local repository to the bitbucket repository. In order to resolve this, I found a [tutorial](https://confluence.atlassian.com/bitbucket/change-the-remote-url-to-your-repository-794212774.html) on how to change the remote URL to my repository. By updating the link of the remote, I was able to push the previous changes as well as any new changes from my local repository to bitbucket.
#### readme.md
I did not know how to create a readme.md file, how to use markdown, or what to include in the file. Some resources that were helpful in creating this were [this tutorial](https://www.makeareadme.com/) as well as [this list of examples](https://github.com/matiassingers/awesome-readme). I had fun learning how to utilize mark down and trying different formats. 


### files included
|file                | description                         |
|--------------------|-------------------------------------|
|[README.MD](https://bitbucket.org/aleisaquach/pic10c-hw1/src/master/README.md)| read me markdown file|
|[cards.h](https://bitbucket.org/aleisaquach/pic10c-hw1/src/master/hw1/cards.h)| class declarations for player, card, and hand. template from Ricardo Salazar |
|[cards.cpp](https://bitbucket.org/aleisaquach/pic10c-hw1/src/master/hw1/cards.cpp)| member function definitions for player, card, and hand. template from Ricardo Salazar |
|[siete-y-medio.cpp](https://bitbucket.org/aleisaquach/pic10c-hw1/src/master/hw1/siete-y-medio.cpp)| main which plays game. template from Ricardo Salazar |

